<?php

namespace App\CoreBundle\Controller;

use App\CoreBundle\Entity\Formulario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AppCoreBundle:Default:index.html.twig', array('name' => $name));
    }




    public function creaCabeceraAction($ruta,
                                       $ciudad,
                                       $dir_ciudad,
                                       $cod_cliente,
                                       $ord_pedido,
                                       $cod_item,
                                       $cantidad,
                                       $cod_vehiculo,
                                       $cod_carga,
                                       $cod_tipo_tramite,
                                       $dir_retiro,
                                       $hora_retiro,
                                       $dir_entrega,
                                       $hora_entrega,
                                       $observacion){


        try{

        $em = $this->getDoctrine()->getManager();

        $obj = new Formulario();
        $obj->setRuta($ruta);
        $obj->setCiudad($ciudad);
        $obj->setDireccionCiudad($dir_ciudad);
        $obj->setCodigoCliente($cod_cliente);
        $obj->setOrdenPedido($ord_pedido);
        $obj->setCodigoItem($cod_item);
        $obj->setCantidad($cantidad);
        $obj->setCodigoVehiculo($cod_vehiculo);
        $obj->setCodigoCarga($cod_carga);
        $obj->setCodigoTipoTramite($cod_tipo_tramite);
        $obj->setDirRetiroCarga($dir_retiro);
        $obj->setHoraRetiro($hora_retiro);
        $obj->setDirEntregaCarga($dir_entrega);
        $obj->setHoraEntrega($hora_entrega);
        $obj->setObservacion($observacion);

        $em->persist( $obj );
        $em->flush();

            $response = new Response(json_encode(array('resultado' => 'true'  , "mensaje" => "exito en el ingreso")));
            $response->headers->set('Content-Type', 'application/json');
            return $response;

      }catch (\Exception $e){
            $response = new Response(json_encode(array('resultado' => 'false'  , "mensaje" => $e->getMessage() )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
}


    }





    public function getCiudadesAction(){
        $em = $this->getDoctrine()->getManager();
        $ciudades = $em->getRepository("AppCoreBundle:Ciudad")->findAll();

        $serializer = $this->container->get('serializer');
        $json= $serializer->serialize($ciudades , 'json');
        return new Response($json);
    }



    public function getVehiculosAction(){
        $em = $this->getDoctrine()->getManager();
        $objects = $em->getRepository("AppCoreBundle:Vehiculo")->findAll();

        $serializer = $this->container->get('serializer');
        $json= $serializer->serialize($objects , 'json');
        return new Response($json);
    }




    public function loginAction( $usuario , $password  ){

        $mensaje = "Login Correcto";
        $bool = true ;
//
//        $user_manager = $this->get('fos_user.user_manager');
//        $factory = $this->get('security.encoder_factory');
//
//        $user = $user_manager->loadUserByUsername($usuario);
//
//
//        $encoder = $factory->getEncoder($user);
//
//        $bool = ($encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt())) ? "true" : "false";
//        if( !$bool ){
//            $mensaje = "Error en las credenciales";
//        }



        $response = new Response(json_encode(array('login' => $bool  , "mensaje" => $mensaje )));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


}
