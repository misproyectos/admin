<?php

namespace App\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class FormularioAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('ruta')
            ->add('ciudad')
            ->add('direccion_ciudad')
            ->add('codigo_cliente')
            ->add('orden_pedido')
            ->add('codigo_item')
            ->add('cantidad')
            ->add('codigo_vehiculo')
            ->add('codigo_carga')
            ->add('codigo_tipo_tramite')
            ->add('dir_retiro_carga')
            ->add('hora_retiro')
            ->add('dir_entrega_carga')
            ->add('hora_entrega')
            ->add('observacion')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            // ->add('ruta')
            ->add('ciudad')
            ->add('direccion_ciudad')
            ->add('codigo_cliente')
            ->add('orden_pedido')
            ->add('codigo_item')
            ->add('cantidad')
            // ->add('codigo_vehiculo')
            // ->add('codigo_carga')
            ->add('codigo_tipo_tramite')
            // ->add('dir_retiro_carga')
            // ->add('hora_retiro')
            // ->add('dir_entrega_carga')
            // ->add('hora_entrega')
            // ->add('observacion')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            
            ->add('ruta')
            ->add('ciudad')
            ->add('direccion_ciudad')
            ->add('codigo_cliente')
            ->add('orden_pedido')
            ->add('codigo_item')
            ->add('cantidad')
            ->add('codigo_vehiculo')
            ->add('codigo_carga')
            ->add('codigo_tipo_tramite')
            ->add('dir_retiro_carga')
            ->add('hora_retiro')
            ->add('dir_entrega_carga')
            ->add('hora_entrega')
            ->add('observacion')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('ruta')
            ->add('ciudad')
            ->add('direccion_ciudad')
            ->add('codigo_cliente')
            ->add('orden_pedido')
            ->add('codigo_item')
            ->add('cantidad')
            ->add('codigo_vehiculo')
            ->add('codigo_carga')
            ->add('codigo_tipo_tramite')
            ->add('dir_retiro_carga')
            ->add('hora_retiro')
            ->add('dir_entrega_carga')
            ->add('hora_entrega')
            ->add('observacion')
        ;
    }
}
