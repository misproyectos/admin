<?php

namespace App\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formulario
 */
class Formulario
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ruta;

    /**
     * @var string
     */
    private $ciudad;

    /**
     * @var string
     */
    private $direccion_ciudad;

    /**
     * @var string
     */
    private $codigo_cliente;

    /**
     * @var integer
     */
    private $orden_pedido;

    /**
     * @var string
     */
    private $codigo_item;

    /**
     * @var integer
     */
    private $cantidad;

    /**
     * @var string
     */
    private $codigo_vehiculo;

    /**
     * @var string
     */
    private $codigo_carga;

    /**
     * @var string
     */
    private $codigo_tipo_tramite;

    /**
     * @var string
     */
    private $dir_retiro_carga;

    /**
     * @var string
     */
    private $hora_retiro;

    /**
     * @var string
     */
    private $dir_entrega_carga;

    /**
     * @var string
     */
    private $hora_entrega;

    /**
     * @var string
     */
    private $observacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     * @return Formulario
     */
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string 
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     * @return Formulario
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string 
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set direccion_ciudad
     *
     * @param string $direccionCiudad
     * @return Formulario
     */
    public function setDireccionCiudad($direccionCiudad)
    {
        $this->direccion_ciudad = $direccionCiudad;

        return $this;
    }

    /**
     * Get direccion_ciudad
     *
     * @return string 
     */
    public function getDireccionCiudad()
    {
        return $this->direccion_ciudad;
    }

    /**
     * Set codigo_cliente
     *
     * @param string $codigoCliente
     * @return Formulario
     */
    public function setCodigoCliente($codigoCliente)
    {
        $this->codigo_cliente = $codigoCliente;

        return $this;
    }

    /**
     * Get codigo_cliente
     *
     * @return string 
     */
    public function getCodigoCliente()
    {
        return $this->codigo_cliente;
    }

    /**
     * Set orden_pedido
     *
     * @param integer $ordenPedido
     * @return Formulario
     */
    public function setOrdenPedido($ordenPedido)
    {
        $this->orden_pedido = $ordenPedido;

        return $this;
    }

    /**
     * Get orden_pedido
     *
     * @return integer 
     */
    public function getOrdenPedido()
    {
        return $this->orden_pedido;
    }

    /**
     * Set codigo_item
     *
     * @param string $codigoItem
     * @return Formulario
     */
    public function setCodigoItem($codigoItem)
    {
        $this->codigo_item = $codigoItem;

        return $this;
    }

    /**
     * Get codigo_item
     *
     * @return string 
     */
    public function getCodigoItem()
    {
        return $this->codigo_item;
    }

    /**
     * Set cantidad
     *
     * @param \integer $cantidad
     * @return Formulario
     */
    public function setCantidad( $cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return \interger 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set codigo_vehiculo
     *
     * @param string $codigoVehiculo
     * @return Formulario
     */
    public function setCodigoVehiculo($codigoVehiculo)
    {
        $this->codigo_vehiculo = $codigoVehiculo;

        return $this;
    }

    /**
     * Get codigo_vehiculo
     *
     * @return string 
     */
    public function getCodigoVehiculo()
    {
        return $this->codigo_vehiculo;
    }

    /**
     * Set codigo_carga
     *
     * @param string $codigoCarga
     * @return Formulario
     */
    public function setCodigoCarga($codigoCarga)
    {
        $this->codigo_carga = $codigoCarga;

        return $this;
    }

    /**
     * Get codigo_carga
     *
     * @return string 
     */
    public function getCodigoCarga()
    {
        return $this->codigo_carga;
    }

    /**
     * Set codigo_tipo_tramite
     *
     * @param string $codigoTipoTramite
     * @return Formulario
     */
    public function setCodigoTipoTramite($codigoTipoTramite)
    {
        $this->codigo_tipo_tramite = $codigoTipoTramite;

        return $this;
    }

    /**
     * Get codigo_tipo_tramite
     *
     * @return string 
     */
    public function getCodigoTipoTramite()
    {
        return $this->codigo_tipo_tramite;
    }

    /**
     * Set dir_retiro_carga
     *
     * @param string $dirRetiroCarga
     * @return Formulario
     */
    public function setDirRetiroCarga($dirRetiroCarga)
    {
        $this->dir_retiro_carga = $dirRetiroCarga;

        return $this;
    }

    /**
     * Get dir_retiro_carga
     *
     * @return string 
     */
    public function getDirRetiroCarga()
    {
        return $this->dir_retiro_carga;
    }

    /**
     * Set hora_retiro
     *
     * @param string $horaRetiro
     * @return Formulario
     */
    public function setHoraRetiro($horaRetiro)
    {
        $this->hora_retiro = $horaRetiro;

        return $this;
    }

    /**
     * Get hora_retiro
     *
     * @return string 
     */
    public function getHoraRetiro()
    {
        return $this->hora_retiro;
    }

    /**
     * Set dir_entrega_carga
     *
     * @param string $dirEntregaCarga
     * @return Formulario
     */
    public function setDirEntregaCarga($dirEntregaCarga)
    {
        $this->dir_entrega_carga = $dirEntregaCarga;

        return $this;
    }

    /**
     * Get dir_entrega_carga
     *
     * @return string 
     */
    public function getDirEntregaCarga()
    {
        return $this->dir_entrega_carga;
    }

    /**
     * Set hora_entrega
     *
     * @param string $horaEntrega
     * @return Formulario
     */
    public function setHoraEntrega($horaEntrega)
    {
        $this->hora_entrega = $horaEntrega;

        return $this;
    }

    /**
     * Get hora_entrega
     *
     * @return string 
     */
    public function getHoraEntrega()
    {
        return $this->hora_entrega;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Formulario
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }
    /**
     * @ORM\PrePersist
     */
    public function preInsert()
    {
        // Add your code here
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        // Add your code here
    }
}
