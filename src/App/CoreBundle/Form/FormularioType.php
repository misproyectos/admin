<?php

namespace App\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormularioType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ruta')
            ->add('ciudad')
            ->add('direccion_ciudad')
            ->add('codigo_cliente')
            ->add('orden_pedido')
            ->add('codigo_item')
            ->add('cantidad')
            ->add('codigo_vehiculo')
            ->add('codigo_carga')
            ->add('codigo_tipo_tramite')
            ->add('dir_retiro_carga')
            ->add('hora_retiro')
            ->add('dir_entrega_carga')
            ->add('hora_entrega')
            ->add('observacion')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\CoreBundle\Entity\Formulario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_corebundle_formulario';
    }
}
